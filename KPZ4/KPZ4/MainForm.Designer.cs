﻿namespace KPZ4
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.button_CreateGraf = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.A_Start = new System.Windows.Forms.TextBox();
            this.T_Start = new System.Windows.Forms.TextBox();
            this.A_Finish = new System.Windows.Forms.TextBox();
            this.T_Finish = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button_Default = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.Iteration = new System.Windows.Forms.TextBox();
            this.panel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.Location = new System.Drawing.Point(439, 13);
            this.pictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(172, 38);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // button_CreateGraf
            // 
            this.button_CreateGraf.Location = new System.Drawing.Point(401, 202);
            this.button_CreateGraf.Margin = new System.Windows.Forms.Padding(4);
            this.button_CreateGraf.Name = "button_CreateGraf";
            this.button_CreateGraf.Size = new System.Drawing.Size(229, 28);
            this.button_CreateGraf.TabIndex = 1;
            this.button_CreateGraf.Text = "Graphic";
            this.button_CreateGraf.UseVisualStyleBackColor = true;
            this.button_CreateGraf.Click += new System.EventHandler(this.button_CreateGraf_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(425, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter the range of coefficients";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(402, 108);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "a: from";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(402, 140);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "t: from";
            // 
            // A_Start
            // 
            this.A_Start.Location = new System.Drawing.Point(454, 104);
            this.A_Start.Margin = new System.Windows.Forms.Padding(4);
            this.A_Start.Name = "A_Start";
            this.A_Start.Size = new System.Drawing.Size(61, 22);
            this.A_Start.TabIndex = 6;
            // 
            // T_Start
            // 
            this.T_Start.Location = new System.Drawing.Point(454, 136);
            this.T_Start.Margin = new System.Windows.Forms.Padding(4);
            this.T_Start.Name = "T_Start";
            this.T_Start.Size = new System.Drawing.Size(61, 22);
            this.T_Start.TabIndex = 8;
            // 
            // A_Finish
            // 
            this.A_Finish.Location = new System.Drawing.Point(569, 104);
            this.A_Finish.Margin = new System.Windows.Forms.Padding(4);
            this.A_Finish.Name = "A_Finish";
            this.A_Finish.Size = new System.Drawing.Size(61, 22);
            this.A_Finish.TabIndex = 10;
            // 
            // T_Finish
            // 
            this.T_Finish.Location = new System.Drawing.Point(569, 136);
            this.T_Finish.Margin = new System.Windows.Forms.Padding(4);
            this.T_Finish.Name = "T_Finish";
            this.T_Finish.Size = new System.Drawing.Size(61, 22);
            this.T_Finish.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(535, 140);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "to";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(535, 108);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "to";
            // 
            // button_Default
            // 
            this.button_Default.Location = new System.Drawing.Point(402, 166);
            this.button_Default.Margin = new System.Windows.Forms.Padding(4);
            this.button_Default.Name = "button_Default";
            this.button_Default.Size = new System.Drawing.Size(229, 28);
            this.button_Default.TabIndex = 16;
            this.button_Default.Text = "Default";
            this.button_Default.UseVisualStyleBackColor = true;
            this.button_Default.Click += new System.EventHandler(this.button_Default_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(399, 72);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Number of points";
            // 
            // Iteration
            // 
            this.Iteration.Location = new System.Drawing.Point(523, 72);
            this.Iteration.Margin = new System.Windows.Forms.Padding(4);
            this.Iteration.Name = "Iteration";
            this.Iteration.Size = new System.Drawing.Size(108, 22);
            this.Iteration.TabIndex = 19;
            // 
            // panel
            // 
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 238);
            this.panel.Margin = new System.Windows.Forms.Padding(4);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1065, 577);
            this.panel.TabIndex = 20;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1065, 815);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.Iteration);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button_Default);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.T_Finish);
            this.Controls.Add(this.A_Finish);
            this.Controls.Add(this.T_Start);
            this.Controls.Add(this.A_Start);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_CreateGraf);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KPZ4";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button button_CreateGraf;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox A_Start;
        private System.Windows.Forms.TextBox T_Start;
        private System.Windows.Forms.TextBox A_Finish;
        private System.Windows.Forms.TextBox T_Finish;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button_Default;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Iteration;
        private System.Windows.Forms.Panel panel;
    }
}

