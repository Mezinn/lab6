﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace KPZ4
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button_Default_Click(object sender, EventArgs e)
        {
            Iteration.Text = Convert.ToString(100);
            A_Start.Text = Convert.ToString(-100);
            T_Start.Text = Convert.ToString(-100);
            A_Finish.Text = Convert.ToString(100);
            T_Finish.Text = Convert.ToString(100);
        }

        private void button_CreateGraf_Click(object sender, EventArgs e)
        {
            int iteration = Convert.ToInt32(Iteration.Text);
            double[,] mas = new double[iteration, 2];

            double a_s, a_f, t_s, t_f;
            a_s = Convert.ToDouble(A_Start.Text);
            t_s = Convert.ToDouble(T_Start.Text);
            a_f = Convert.ToDouble(A_Finish.Text);
            t_f = Convert.ToDouble(T_Finish.Text);

            double stepA = (Math.Abs(a_s) + Math.Abs(a_f)) / iteration;
            double stepT = (Math.Abs(t_s) + Math.Abs(t_f)) / iteration;

            for (int i = 0; i < iteration; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (j == 0)
                    {
                        mas[i, j] = Calc_X(a_s, t_s);
                        a_s += stepA;
                        t_s += stepT;
                    }
                    if (j == 1)
                    {
                        mas[i, j] = Calc_Y(a_s, t_s);
                        a_s += stepA;
                        t_s += stepT;
                    }
                }
            }
            float xMin, xMax, yMin, yMax, x, y;
            xMin = yMin = float.PositiveInfinity;
            xMax = yMax = float.NegativeInfinity;
            for (int i = 0; i < iteration; i++)
            {
                x = (float)mas[i, 0];
                y = (float)mas[i, 1];
                if (xMin > x) xMin = x;
                if (xMax < x) xMax = x;
                if (yMin > y) yMin = y;
                if (yMax < y) yMax = y;
            }
            // Вычисляем необходимый масштаб.
            float scaleX = (float)(panel.Width - 1f) / (xMax - xMin); // Добавил -1, чтобы все штрихи влазили.
            float scaleY = (float)(panel.Height - 1f) / (yMax - yMin); // Добавил -1, чтобы все штрихи влазили.
                                                                       // Вычисляем смещение центра осей (или что-то типо того):
            float shiftX = xMin < 0 ? -xMin : xMin; // Вообще это описание модуля Math.Abs(value), но я не хочу приводить типы.
            float shiftY = yMin < 0 ? -yMin : yMin;

            Pen pen = new Pen(new SolidBrush(Color.Red), 2f); // Рисую график толстыми красными линиями.
            Pen grid = new Pen(new SolidBrush(Color.Black), 1f); // Рисую ось, ее штрихи и текст черным.
            Graphics graphics = panel.CreateGraphics(); // Рисую график на Panel.

            // Центр осей координат:
            float centerX = shiftX * scaleX;
            float centerY = shiftY * scaleY;

            // Рисую ось:
            graphics.DrawLine(grid, centerX, 0, centerX, panel.Height);
            graphics.DrawLine(grid, 0, centerY, panel.Width, centerY);

            int countX = 5; // Количество штрихов на оси по X в сторону.
            int countY = 2; // Количество штрихов на оси по Y в сторону.
            float plen = 2f; // Половина длины штриха.

            // Описываем шрифт:
            Font font = new Font(new FontFamily("Arial"), 10); // Сам шрифт.
            Brush fontBrush = new SolidBrush(Color.Green); // Цвет текста.
            float rangeFont = 2f; // Длина промежутка между текстом и штрихом.
            SizeF sizeFont; // Занимаемое место шрифтов.
            int point = 2; // Количество цифр после запятой.

            // Промежуточные переменные:
            Image image;
            Graphics temp;

            // Рисую штрихи на оси X:
            float stepX = centerX / countX; // Размер шага между штрихами по Y.
            float stepValueX = (float)Math.Max(Math.Abs(xMax), Math.Abs(xMin)) / countX; // Размер шага для значений по X.
                                                                                         //float stepValue
            for (int i = 1; i <= countX; i++)
            {
                // Штрихи влево:
                graphics.DrawLine(grid, centerX - stepX * i, centerY - plen, centerX - stepX * i, centerY + plen);
                // Штрихи вправо:
                graphics.DrawLine(grid, centerX + stepX * i, centerY - plen, centerX + stepX * i, centerY + plen);

                // Текст влево:
                sizeFont = graphics.MeasureString(Math.Round(stepValueX * i, point).ToString(), font);
                image = new Bitmap((int)sizeFont.Width + 1, (int)sizeFont.Height + 1); // +1 для того, чтобы все влезло.
                temp = Graphics.FromImage(image);
                temp.DrawString(Math.Round(stepValueX * i, point).ToString(), font, fontBrush, 0f, 0f);
                image.RotateFlip(RotateFlipType.Rotate90FlipXY);
                graphics.DrawImageUnscaled(image, (int)(centerX - stepX * i), (int)(centerY - sizeFont.Width - plen - rangeFont));
                // Текст вправо:
                sizeFont = graphics.MeasureString(Math.Round(-stepValueX * i, point).ToString(), font);
                image = new Bitmap((int)sizeFont.Width + 1, (int)sizeFont.Height + 1); // +1 для того, чтобы все влезло.
                temp = Graphics.FromImage(image);
                temp.DrawString(Math.Round(-stepValueX * i, point).ToString(), font, fontBrush, 0f, 0f);
                image.RotateFlip(RotateFlipType.Rotate270FlipXY);
                graphics.DrawImageUnscaled(image, (int)(centerX + stepX * i - sizeFont.Height), (int)(centerY + plen + rangeFont));
            }

            // Рисую штрихи на оси Y:
            float stepY = centerY / countY; // Размер шага между штрихами по Y.
            float stepValueY = (float)Math.Max(Math.Abs(yMax), Math.Abs(yMin)) / countY; // Размер шага для значений по Y.
            for (int i = 1; i <= countY; i++)
            {
                // Штрихи вверх:
                graphics.DrawLine(grid, centerX - plen, centerY - stepY * i, centerX + plen, centerY - stepY * i);
                // Штрихи вниз:
                graphics.DrawLine(grid, centerX - plen, centerY + stepY * i, centerX + plen, centerY + stepY * i);

                // Текст вверх:
                graphics.DrawString(Math.Round(stepValueY * i, point).ToString(), font, fontBrush,
              centerX + plen + rangeFont, centerY - stepY * i);
                // Текст вниз:
                sizeFont = graphics.MeasureString(Math.Round(-stepValueY * i, point).ToString(), font);
                graphics.DrawString(Math.Round(-stepValueY * i, point).ToString(), font, fontBrush,
                  centerX - plen - rangeFont - sizeFont.Width, centerY + stepY * i - sizeFont.Height);
            }

            for (int i = 1; i < iteration; i++) // Рисую сам график по точкам.
                graphics.DrawLine(pen, ((float)mas[i - 1, 0] + shiftX) * scaleX,
                panel.Height - ((float)mas[i - 1, 1] + shiftY) * scaleY,
                ((float)mas[i, 0] + shiftX) * scaleX,
                panel.Height - ((float)mas[i, 1] + shiftY) * scaleY);
        }

        private double Calc_X(double A, double T)
        {
            return (A*Math.Tan(T));
        }

        private double Calc_Y(double A, double T)
        {
            return (A*Math.Pow(Math.Sin(T),2));
        }
    }
}